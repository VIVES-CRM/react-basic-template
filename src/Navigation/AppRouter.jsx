import React from 'react'
import { Switch, Route } from 'react-router-dom'

import WelcomeScreen from '../Screens/WelcomeScreen'
import DetailScreen from '../Screens/DetailScreen'

const AppRouter = () => (
  <Switch>
    <Route path="/" exact component={WelcomeScreen} />
    <Route path="/detail/" component={DetailScreen} />
  </Switch>
)

export default AppRouter
