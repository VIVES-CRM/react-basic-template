import React from 'react'
import { BrowserRouter, NavLink } from 'react-router-dom'

import AppRouter from './AppRouter'
import '../App.css'

const Navigate = () => (
  <BrowserRouter>
    <div className="body">
      <nav className="navigate">
        <ul>

          <li>
            <NavLink exact to="/" activeClassName="activeNavLink">Welcome</NavLink>
          </li>

          <li>
            <NavLink to="/detail/" activeClassName="activeNavLink">Detail</NavLink>
          </li>

        </ul>
      </nav>

      <hr />
      <br />

      <AppRouter />

    </div>
  </BrowserRouter>
)

export default Navigate
