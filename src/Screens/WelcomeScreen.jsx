import React from 'react'
import { Link } from 'react-router-dom'

const WelcomeScreen = () => (
  <>
    <h1>
      Hello World from React Basic Template
    </h1>
    <hr />
    <br />
    <br />
    <h3>
      Ga naar de&nbsp;
      <Link to="/detail/">detail pagina</Link>
    </h3>
    <br />
    <hr />
    <h4>Renaat Meeuws</h4>
    <hr />
  </>
)

export default WelcomeScreen
