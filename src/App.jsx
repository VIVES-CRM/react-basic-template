import React from 'react'
import { hot } from 'react-hot-loader'
import Navigate from './Navigation/Navigate'


const App = () => <Navigate />


export default hot(module)(App)
